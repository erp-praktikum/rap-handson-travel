sap.ui.define(
  ["sap/ui/core/format/DateFormat", "sap/ui/model/Filter"],
  function (DateFormat, Filter) {
    "use strict";

    return sap.ui.controller("zraphtravel.ext.controller.ListReportExt", {
      /**
       * Event handler called before the table is rebinded.
       * We adapt the binding
       * @param {sap.ui.base.Event} oEvent rebind table event
       * @public
       */
      onBeforeRebindTableExtension: function (oEvent) {
        var oBindingParams = oEvent.getParameter("bindingParams");
        oBindingParams.parameters = oBindingParams.parameters || {};

        var oSmartFilterBar = this.byId(oEvent.getSource().getSmartFilterId()),
          oDate = oSmartFilterBar
            .getControlByKey("CustomValidityDateFilter")
            .getDateValue(),
          aFilters = oBindingParams.filters;

        if (oDate !== null) {
          var oUTC = DateFormat.getDateInstance({ UTC: true });
          var oDateUTC = oUTC.parse(
            oUTC.format(new Date(oDate), false),
            true,
            false
          );

          aFilters.push(new Filter("BeginDate", "LE", oDateUTC));
          aFilters.push(new Filter("EndDate", "GE", oDateUTC));
        }

        //apply filters
        if (aFilters.length > 0) {
          oBindingParams.filters = [new Filter(aFilters, true)];
        }
      }
    });
  }
);
