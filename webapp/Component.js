jQuery.sap.declare("zraphtravel.Component");
sap.ui.getCore().loadLibrary("sap.ui.generic.app");
jQuery.sap.require("sap.ui.generic.app.AppComponent");

sap.ui.define(["sap/suite/ui/generic/template/lib/AppComponent"], function (
  AppComponent
) {
  return AppComponent.extend("zraphtravel.Component", {
    metadata: {
      manifest: "json"
    }
  });
});
