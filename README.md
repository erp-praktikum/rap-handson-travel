## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Fri Nov 13 2020 11:58:28 GMT+0200 (Ora standard a Europei de Est)|
|**App Generator Version**<br>1.0.21|
|**Generation Platform**<br>Visual Studio Code|
|**Template Used**<br>List Report Object Page V2|
|**Service Type**<br>OData Url|
|**Service URL**<br>https://vhosts4d.awscloud.msg.de:8443/sap/opu/odata/sap/ZRAPH_UI_TRAVEL_U_V2/|
|**Main Entity**<br>Travel|
|**Navigation Entity**<br>to_Booking|
|**Module Name**<br>zraph_travel|
|**Application Title**<br>Travel Management|
|**Namespace**<br>|
|**UI5 Theme**<br>sap_fiori_3|
|**UI5 Version**<br>Latest |
|**Enable Telemetry**<br>True |

## zraph_travel

Travel Management

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply run the following from the generated app root folder:

```
    npm start
```

- It is also possible to run the application using mock data that reflects the OData Service URL supplied during application generation.  In order to run the application with Mock Data, run the following from the generated app root folder:

```
    npm run start-mock
```


#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


